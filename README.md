# Objetivo do sistema
Calcular o custo total de um determinado período de serviço prestado por uma empresa. 


## O sistema
O sistema consiste em uma API Empresa e outra API para cálculo de custo por período de serviço prestado.

Nele tu poderá realizar o CRUD de uma empresa, o CRUD de um feriado e consultar o custo total por um determinado período de serviço, levando em consideração a carga horária diária de 8 horas e feriado(s).


## Tecnologias utilizadas
RESTful usando Node.js, Express, Mysql, Sequelize.


## Arquitetura
Para a criação do projeto foi utilizado o padrão arquitetural de software MVC.


## Siga os passos abaixo para executar os projetos
1) Faça o clone dos dois projetos disponíveis no grupo deste projeto, Company_api e Calculation_api
2) Crie um banco de dados com o nome: _empresa_gabriel_
3) No projeto Company_api, defina os seguintes dados no arquivo .env.example:
- A porta em que o servidor irá rodar
- Host do banco de dados
- O nome do banco de dados
- A porta do banco de dados
- A senha do banco de dados
- O usuário do bando de dados
4) No projeto Calculation_api, defina os seguintes dados no arquivo .env.example:
-  A porta em que o servidor irá rodar
- A URL base em que o projeto Company_api irá rodar
5) Em ambos projetos, renomeie o arquivo na raiz chamado de .env.example para .env
6) Em ambos projetos, inicie as dependências com o comando
```
npm install
```
7) No projeto Company_api, crie as tabelas do banco de dados executando as migrações com o comando
```
npm run migrate
```
8) _OPICIONAL_ - No projeto Company_api, caso queira você pode popular o banco de dados com as seeds fornecidas, para isto execute o comando
```
npm run seed
```
9) Em ambos projetos, execute o seguinte comando para iniciar o servidor
```
npm run dev 
```

_Vide seção scripts em package.json na raiz do projeto Company_api para conhecimento de comandos auxiliares_

## Postman
Para consumo das API's veja a documentação no **Postman**

[**Documentation in Postman**](https://documenter.getpostman.com/view/16658273/2s7YmrjmAX)

**_Coleção disponível na raiz deste projeto de documentação das API's_**

---


## Estrutura de diretórios do projeto Company_api
```
├── /src
|   ├── /controllers
|   ├── /database
|   |   ├── /config
|   |   ├── /migrations
|   |   ├── /seeders
|   ├── /helpers
|   ├── /middlewares
|   ├── /models
|   ├── /routes
```


## Estrutura de diretórios do projeto Calculation_api
```
├── /src
|   ├── /controllers
|   ├── /helpers
|   ├── /http
|   ├── /middlewares
|   ├── /routes
|   ├── /services
```


---

## Modelo relacional do banco de dados do projeto Company_api
![tables](https://user-images.githubusercontent.com/63760217/189154558-f1bb7cd0-ffb7-4366-9257-40371e3d69fc.png)
